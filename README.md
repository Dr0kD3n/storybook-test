# StoryBook

## Copy repository
```
git clone https://gitlab.com/Dr0kD3n/storybook-test.git
```

## Navigate to directory and install dependencies
```
cd storybook-test && npm install
```

## Start development server
```
npm run storybook
```
